// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
#ifndef SPACE_HEMISPHERE_H
#define SPACE_HEMISPHERE_H

#include "space.h"

/// sphere centered at the origin.
/**
 Space `hemisphere` is a sphere centered around the origin, with one flat side.
 Cutoff gives the z distance from the center where the sphere is cut. The value
 can also be initialized as a fraction of the radius.
    Thus, cutoff = 0 or fraction = 0.5 give a traditional hemisphere.
 The corners where the plane meets the sphere are interpolated by projecting a 
 smaller circle in this corner. The circle is chosen such that the shape is continuous
 and there is never a jump in the derivative or position of the shape. 

 
 Parameters:
    - radius = radius of the sphere
    - cutoff = the distance z to the center at which the side is flat
    -interpolation_distance = the distance from the cutoff plane that the interpolation of the corner starts 
                                            (this is not the radius of the interpolating circle)
    .
 
 @ingroup SpaceGroup
 */

class SpaceHemisphere : public Space
{
protected:
    
    /// the radius of the sphere
    real  radius_;

    /// the z of the flat side of the sphere
    real cutoff_;

    /// the distance of the start of interpolating region from the plane
    real interpolation_dist_;
    
    /// the corner of the interpolation circle in 2D
    Vector2 c_corner_;

    /// the radius of the interpolation circle
    real r_corner_;

    /// square of the radius
    real  radiusSqr_;
    
    /// calculate radiusSqr
    void  update(); 
    
public:
    
    /// constructor
    SpaceHemisphere(SpaceProp const*);

    /// check number and validity of specified lengths
    void        resize(Glossary& opt);

    /// return bounding box in `inf` and `sup`
    void        boundaries(Vector& inf, Vector& sup) const;
    
    /// true if the point is inside the Space
    bool        inside(Vector const&) const;

    /// a random position located on the edge
    Vector         randomPlaceOnEdge(real rad, size_t nb_trials) {return project(randomPlaceNearEdge(r_corner_, nb_trials));};

    /// set `proj` as the point on the edge that is closest to `point`
    Vector      project(Vector const& pos) const;
 
    /// OpenGL display function; returns true if successful
    bool        draw() const;
    
    /// write to file
    void        write(Outputter&) const;

    /// get dimensions from array `len`
    void        setLengths(const real len[8]);
    
    /// read from file
    void        read(Inputter&, Simul&, ObjectTag);

};

#endif

