// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
#include "space_cylinderY.h"
#include "exceptions.h"
#include "iowrapper.h"
#include "mecapoint.h"
#include "glossary.h"
#include "meca.h"


SpaceCylinderY::SpaceCylinderY(SpaceProp const* p)
: Space(p)
{
    if ( DIM < 3 )
        throw InvalidParameter("cylinderY is only valid in 3D: use sphere instead");
    bot_ = 0;
    top_ = 0;
    radius_ = 0;
}


void SpaceCylinderY::resize(Glossary& opt)
{
    real rad = radius_, top = top_, bot = bot_;

    if ( opt.set(rad, "diameter") )
        rad *= 0.5;
    else opt.set(rad, "radius");
    opt.set(bot, "bottom");
    opt.set(top, "top");
    
    if ( rad < 0 )
        throw InvalidParameter("cylinderY:radius must be >= 0");

    if ( top < bot )
        throw InvalidParameter("cylinerY:bottom must be <= top");
    
    bot_ = bot;
    top_ = top;
    radius_ = rad;
}


void SpaceCylinderY::boundaries(Vector& inf, Vector& sup) const
{
    inf.set(-radius_, bot_, -radius_);
    sup.set( radius_, top_, radius_);
}


real  SpaceCylinderY::volume() const
{
    return M_PI * ( top_ - bot_ ) * radius_ * radius_;
}


bool  SpaceCylinderY::inside(Vector const& w) const
{
#if ( DIM > 2 )
    if ( w.YY < bot_ ) return false;
    if ( w.YY > top_ ) return false;
    return ( w.XX*w.XX + w.ZZ*w.ZZ <= radius_ * radius_ );
#else
    return ( fabs(w.XX) <= radius_ );
#endif
}

bool  SpaceCylinderY::allInside(Vector const& w, const real rad ) const
{
    assert_true( rad >= 0 );
#if ( DIM > 2 )
    if ( w.YY - rad < bot_ ) return false;
    if ( w.YY + rad > top_ ) return false;
    return ( w.XX*w.XX + w.ZZ*w.ZZ <= square(radius_-rad) );
#else
    return ( fabs(w.XX) <= radius_-rad );
#endif
}

Vector SpaceCylinderY::randomPlace() const
{
    const Vector2 V = Vector2::randB(radius_);
    return Vector(V.XX, bot_+RNG.preal()*(top_-bot_), V.YY);
}

//------------------------------------------------------------------------------
Vector SpaceCylinderY::project(Vector const& w) const
{
    Vector p = w;
#if ( DIM >= 3 )
    bool inY = true;
    
    if ( w.YY > top_ )
    {
        p.YY = top_;
        inY = false;
    }
    else if ( w.YY < bot_ )
    {
        p.YY = bot_;
        inY = false;
    }

    real n = w.normXZ();
    
    if ( n > radius_ )
    {
        n = radius_ / n;
        p.XX = n * w.XX;
        p.ZZ = n * w.ZZ;
    }
    else
    {
        if ( inY )
        {
            if ( top_ - w.YY < radius_ - n )
                p.YY = top_;
            else if ( w.YY - bot_ < radius_ - n )
                p.YY = bot_;
            else
            {
                n = radius_ / n;
                p.XX = n * w.XX;
                p.ZZ = n * w.ZZ;
            }
        }
    }
#endif
    return p;
}

//------------------------------------------------------------------------------

/**
 This applies the correct forces in the cylindrical and spherical parts.
 */
void SpaceCylinderY::setInteraction(Vector const& pos, Mecapoint const& pe,
                                    Meca & meca, real stiff,
                                    const real rad, const real B, const real T)
{
#if ( DIM >= 3 )
    bool cap = false;
    bool cyl = false;
    real Y;

    // inside cylinder radius_
    if ( 2 * pos.YY - B > T )
    {
        Y = T;
        cap = ( pos.YY > T );
    }
    else
    {
        Y = B;
        cap = ( pos.YY < B );
    }
    
    real dis = pos.XX*pos.XX + pos.ZZ*pos.ZZ;
    
    if ( rad*rad < dis )
    {
        // outside cylinder in XY plane
        cyl = true;
    }
    else if ( ! cap )
    {
        // inside cylinder in XY plane and also inside in Z:
        if ( dis > square( rad - fabs(pos.YY-Y) ) )
            cyl = true;
        else
            cap = true;
    }
    
    if ( cap )
    {
        const index_t inx = 1 + DIM * pe.matIndex();
        meca.mC(inx, inx) -= stiff;
        meca.base(inx)    += stiff * Y;
    }
    
    if ( cyl )
        meca.addCylinderClampY(pe, rad, stiff);
#endif
}


/**
 This applies the correct forces in the cylindrical and spherical parts.
 */
void SpaceCylinderY::setInteraction(Vector const& pos, Mecapoint const& pe, Meca & meca, real stiff) const
{
    setInteraction(pos, pe, meca, stiff, radius_, bot_, top_);
}

/**
 This applies the correct forces in the cylindrical and spherical parts.
 */
void SpaceCylinderY::setInteraction(Vector const& pos, Mecapoint const& pe, real rad, Meca & meca, real stiff) const
{
    real R = std::max((real)0, radius_ - rad);
    real T = top_ - rad;
    real B = bot_ + rad;
    
    if ( B > T )
    {
        B = 0.5 * ( top_ + bot_ );
        T = B;
    }
    
    setInteraction(pos, pe, meca, stiff, R, B, T);
}


//------------------------------------------------------------------------------

void SpaceCylinderY::write(Outputter& out) const
{
    out.put_characters("cylinderY", 16);
    out.writeUInt16(4);
    out.writeFloat(radius_);
    out.writeFloat(bot_);
    out.writeFloat(top_);
    out.writeFloat(0.f);
}


void SpaceCylinderY::setLengths(const real len[])
{
    radius_ = len[0];
    bot_    = len[1];
    top_    = len[2];
}

void SpaceCylinderY::read(Inputter& in, Simul&, ObjectTag)
{
    real len[8] = { 0 };
    read_data(in, len, "cylinderY");
    setLengths(len);
}

//------------------------------------------------------------------------------
//                         OPENGL  DISPLAY
//------------------------------------------------------------------------------

#ifdef DISPLAY
#include "opengl.h"
#include "gle.h"

bool SpaceCylinderY::draw() const
{
#if ( DIM > 2 )
    
    GLfloat T = top_;
    GLfloat B = bot_;
    GLfloat R = radius_;
    
    const size_t fin = 512;
    GLfloat c[fin+1], s[fin+1];
    gle::circle(fin, c, s, 1);
    
    glBegin(GL_TRIANGLE_STRIP);
    //display strips along the side of the volume:
    for ( size_t n = 0; n <= fin; ++n )
    {
        glNormal3f(c[n], 0, s[n]);
        glVertex3f(R*c[n], T, R*s[n]);
        glVertex3f(R*c[n], B, R*s[n]);
    }
    glEnd();
    
    // draw top cap:
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, +1, 0);
    glVertex3f(0, T,  0);
    for ( size_t n = 0; n <= fin; ++n )
        glVertex3f(R*c[n],T, R*s[n]);
    glEnd();
    
    // draw bottom cap:
    glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, -1, 0);
    glVertex3f(0, B,  0);
    for ( size_t n = 0; n <= fin; ++n )
        glVertex3f(-R*c[n], B, R*s[n]);
    glEnd();
    
#endif
    return true;
}

#else

bool SpaceCylinderY::draw() const
{
    return false;
}

#endif
