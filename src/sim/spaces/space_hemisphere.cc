// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
#include "dim.h"
#include "space_hemisphere.h"
#include "exceptions.h"
#include "iowrapper.h"
#include "glossary.h"
#include "random.h"
#include "meca.h"


SpaceHemisphere::SpaceHemisphere(SpaceProp const* p)
: Space(p), radius_(0), cutoff_(0), radiusSqr_(0)
{
    if ( DIM < 2 )
        throw InvalidParameter("cylinderY is only valid in 2D and 3D");
}


void SpaceHemisphere::resize(Glossary& opt)
{
    real rad = radius_;
    real cut = cutoff_;
    real interpolation_dist; 
    
    if ( opt.set(rad, "diameter") )
        rad *= 0.5;
    else opt.set(rad, "radius");
    
    if ( rad < 0 )
        throw InvalidParameter(prop->name()+":radius must be >= 0");

    if ( opt.set(cut, "fraction") ){
         cut *= rad*2;
         cut -= rad;
    } else if ( !opt.set(cut, "cutoff") )
        cut = 0;

    if (opt.set(interpolation_dist, "interpolation")){
        interpolation_dist *= rad - cut;
    } else if (!opt.set(interpolation_dist, "interpolation_distance")){
        interpolation_dist = 0.1;
    }
        
    if ( cut < -rad || cut > rad )
        throw InvalidParameter(prop->name()+":the cutoff needs to be inside the sphere.");

    if (interpolation_dist < 0 || interpolation_dist >= rad*2-fabs(rad-cut)) 
        throw InvalidParameter(prop->name()+": the interpolation distance needs to be positive and smaller than the radius - cutoff");
    
    radius_ = rad;
    cutoff_ = cut;
    interpolation_dist_ = interpolation_dist;

    update();
}


void SpaceHemisphere::update(){
    radiusSqr_ = square(radius_); 

    if (interpolation_dist_ == 0){
        // initializes unreal values such that the interpolation region will never be found.
        c_corner_= Vector2( (std::sqrt(square(radius_)-pow(cutoff_, 2)) ) + 1, cutoff_ + 1);
        r_corner_=0;
    }

    // a point interpolation_dist away from the flat plane on the sphere
    Vector2 start_interpolate = Vector2( (std::sqrt(square(radius_)-square(cutoff_-interpolation_dist_)) ) , cutoff_-interpolation_dist_);

    // finds the point c_corner_ on the line origin to start_interpolate where it can form a circle with right angles to both the circle and plane
    real beta = (M_PI_2 + start_interpolate.spherical().y())/2;
    //force isosceles triangle between c_corner, and start and end of interpolated area -> ensures continuity of shape
    real x_a =start_interpolate.x() - tan(beta) * (cutoff_ - start_interpolate.y()); 
    c_corner_ = Vector2(x_a, (start_interpolate.y()/start_interpolate.x())*x_a);

    // radius of interpolation circle
    r_corner_  = cutoff_ - c_corner_.y();

    #if (0 && !DISPLAY)
    if (interpolation_dist_>0){
        std::clog << "The interpolated region of " << prop->name() << " is a circle from: "<< std::endl;
        c_corner_.println();
        std::clog << "with radius: " << r_corner_ << std::endl;
    }
    #endif
}


void SpaceHemisphere::boundaries(Vector& inf, Vector& sup) const
{
    inf.set(-radius_,-radius_,-radius_);
    sup.set( radius_, cutoff_, radius_);
}

bool SpaceHemisphere::inside(Vector const& pos) const
{
    if (!(pos.normSqr() <= radiusSqr_ && pos.y() <= cutoff_)){ 
        return false;
    }
    Vector2 pos_2d = Vector2(sqrt(square(pos.x()) + square(pos.z())), pos.y()); // 2D in the direction of pos from origin
    if (pos_2d.x() > c_corner_.x() && cross(pos_2d,c_corner_) < 0 ){
        return (pos_2d-c_corner_).normSqr() <= square(r_corner_);
    }
    return true ;
}

Vector SpaceHemisphere::project(Vector const& pos) const
{    
    // this code can be split to make 2D faster
    real n = pos.norm();
    Vector2 pos_2d = Vector2(sqrt(square(pos.x()) + square(pos.z())), pos.y()); // 2D in the direction of pos from origin
    if (n > 0){
        if (cross(pos_2d,c_corner_) > 0 && n-radius_ > pos.y() - cutoff_){
            return pos *  radius_ / n ;
        } else if (pos_2d.x() < c_corner_.x()) {
            return Vector(pos.x(), cutoff_, pos.z());
        } else { // translates to center in a 2D plane aligned with pos, projects onto a smaller circle, and translates back
            Vector2 prj2 = (pos_2d - c_corner_)*(r_corner_/(pos_2d - c_corner_).norm()) + c_corner_;
#if (DIM == 2)
            if (pos.x() < 0) prj2.XX *=-1;
            return prj2;
#elif (DIM == 3) // rotates 2D back into 3D
            return Vector(prj2.x() * pos.x() /pos_2d.x(), prj2.y(), prj2.x() * pos.z() / pos_2d.x());
#endif
        } 
    } else {
        return project(Vector::randU(0.0001));
    }
}

//------------------------------------------------------------------------------

void SpaceHemisphere::write(Outputter& out) const
{
    out.put_characters("hemisphere", 16);
    out.writeUInt16(3);
    out.writeFloat(radius_);
    out.writeFloat(cutoff_);
    out.writeFloat(interpolation_dist_);
}


void SpaceHemisphere::setLengths(const real len[])
{
    radius_ = len[0];
    cutoff_ = len[1];
    interpolation_dist_ = len[2];
    update();
}


void SpaceHemisphere::read(Inputter& in, Simul&, ObjectTag)
{
    real len[8] = { 0 };
    read_data(in, len, "hemisphere");
    setLengths(len);
}


//------------------------------------------------------------------------------
//                         OPENGL  DISPLAY
//------------------------------------------------------------------------------

#ifdef DISPLAY

#include "gle.h"

bool SpaceHemisphere::draw() const
{
    //number of sections in the quarter-circle
    constexpr size_t fin = 8 * gle::finesse;
    Vector pts[fin];
    for (int i = 0; i <fin; i++){
        pts[i]= project(Vector(radius_ *2 , (i+1)*(M_PI)/fin -M_PI_2, M_PI_2).cartesian());
    }
    //naively project()s onto a wide radius
#if (DIM == 2)
    glBegin(GL_LINE_LOOP);
    for(int i = 0; i<fin; i++){ gle::gleVertex(pts[i].x(), pts[i].y());}
    for(int i = fin-1; i>0; i--){ gle::gleVertex(-pts[i].x(), pts[i].y());}
    glEnd();
    
#elif (DIM == 3)
    GLfloat c[fin+1], s[fin+1];
    gle::circle(fin, c, s, 1);
    
    //display surface
    glBegin(GL_TRIANGLE_FAN);
    glVertex3f(0, -radius_,0);
    for ( size_t p = 0; p <= fin; ++p )
    {
        glNormal3f(GLfloat(radius_- pts[1].y()) *c[p], GLfloat(-pts[1].x()) ,GLfloat(radius_- pts[1].y())*s[p]);
        glVertex3f( pts[1].x()*c[p], pts[1].y(), pts[1].x() *s[p] );
    }
    glEnd();

    for ( unsigned n=1; n < fin; n++ )
    {
        // do not display special edges
        GLfloat R1 = GLfloat(pts[n-1].x());
        GLfloat Y1 = GLfloat(pts[n-1].y());
        GLfloat R2 = GLfloat(pts[n].x());
        GLfloat Y2 = GLfloat(pts[n].y());
        
        GLfloat nX = GLfloat( pts[n].y()-pts[n-1].y());
        GLfloat nY = GLfloat(pts[n-1].x()-pts[n].x());
        
        glBegin(GL_TRIANGLE_STRIP);
        for ( size_t p = 0; p <= fin; ++p )
        {
            glNormal3f(nX*c[p], nY, nX*s[p]);
            glVertex3f(pts[n-1].x()*c[p], pts[n-1].y(), pts[n-1].x()*s[p]);
            glVertex3f(pts[n].x()*c[p], pts[n].y(), pts[n].x()*s[p]);
        }
        glEnd();   
    }
#endif
    return true;
}

#else

bool SpaceHemisphere::draw() const
{
    return false;
}

#endif
