// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.

#ifndef CAPPER_H
#define CAPPER_H

#include "hand.h"
class CapperProp;

/// a Hand that can move smoothly on a Fiber
/**
 The Capper is a Hand, and thus can bind and unbind from fibers.
 
 A bound Capper can move along its fiber and halt dynamics at one of the ends. 
 Hold_onto_end is no longer a used variable as this is hardcoded to True for this class.

 The Capper sets the tip state at which it arrives to STATE_WHITE.
 When the Capper unbinds it sets the tip to default STATE_RED, or a user set release_state.
  The Capper will not pause/cap a fiber in STATE_RED, to prevent rescue. 

 The direction of the Capper is set by the sign of @ref CapperPar "unloaded_speed".
 If the speed is positive, the Capper attempts to move towards the PLUS_END.
 
 The speed is linearly proportional to the load of the Capper.
 The load is the projection of the force vector on the direction of the fiber.
 
     real load = force * direction_of_fiber;
     real speed = unloaded_speed * ( 1 + load / stall_force );
 
 The actual movement depends on the time step:

     real displacement = speed * time_step;
 
 As defined in Hand, detachment increases exponentially with force.

 See Examples and the @ref CapperPar.
 @ingroup HandGroup
 */
class Capper : public Hand
{
private:
    
    /// disabled default constructor
    Capper();
    
    /// clamp a in [0,b]
    void limitSpeedRange(real& a, const real b);

    // redefines detachment to include setting the state of the fiber to release_state
    bool testDetachment();
    bool testKramersDetachment(const real force);

public:
    
    /// Property
    CapperProp const* prop;
    
    /// constructor
    Capper(CapperProp const*, HandMonitor*);
    
    /// destructor
    ~Capper() {}
    
    
    /// simulate when `this` is attached but not under load
    void   stepUnloaded();
    
    /// simulate when `this` is attached and under load
    void   stepLoaded(Vector const& force, real force_norm);
    
};

#endif

