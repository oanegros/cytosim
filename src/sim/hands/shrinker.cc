// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.

#include "shrinker.h"
#include "shrinker_prop.h"
#include "glossary.h"
#include "exceptions.h"
#include "iowrapper.h"
#include "simul.h"


Shrinker::Shrinker(ShrinkerProp const* p, HandMonitor* h)
: Hand(p,h), prop(p)
{
    engaged = NO_END;
}


void Shrinker::attach(FiberSite const& s)
{
    engaged = NO_END;
    Hand::attach(s);
}


void Shrinker::stepUnloaded()
{
    assert_true( attached() );
    
    // test for detachment
    if ( testDetachment() )
        return;
    
    if ( engaged != NO_END )
    {
#if NEW_FIBER_CHEW
        fbFiber->chew(prop->set_speed_dt, engaged);
        moveToEnd(engaged);
#else
        throw InvalidParameter("fiber:chew is not enabled");
#endif
        return;
    }

    real a = fbAbs + prop->diffusion_dt * RNG.sreal();
    
    if ( a <= fbFiber->abscissaM() )
    {
        a = fbFiber->abscissaM();
        engaged = MINUS_END;
    }
    
    if ( a >= fbFiber->abscissaP() )
    {
        a = fbFiber->abscissaP();
        engaged = PLUS_END;
    }
    
    if ( engaged && RNG.test_not(prop->hold_growing_end) )
        detach();
    else
        moveTo(a);
}


void Shrinker::stepLoaded(Vector const& force, real force_norm)
{
    assert_true( attached() );
    assert_true( nextDetach >= 0 );
    
    if ( testKramersDetachment(force_norm) )
        return;
    
    real load = dot(force, dirFiber());

    if ( engaged != NO_END )
    {
#if NEW_FIBER_CHEW
        // calculate load-dependent displacement:
        real dab = prop->set_speed_dt + load * prop->var_speed_dt;
        fbFiber->chew(dab, engaged);
        moveToEnd(engaged);
#else
        throw InvalidParameter("fiber:chew is not enabled");
#endif
        return;
    }
    
    // the load is the projection of the force on the local direction of Fiber
    real a = fbAbs + prop->diffusion_dt * RNG.sreal() + prop->mobility_dt * load;
    
    const real m = fbFiber->abscissaM();
    const real p = fbFiber->abscissaP();
    
    if ( a <= m + prop->bind_end_range )
    {
        a = m;
        engaged = MINUS_END;
    }
    
    if ( a >= p - prop->bind_end_range)
    {
        a = p;
        engaged = PLUS_END;
    }
    
    if ( engaged && RNG.test_not(prop->hold_growing_end) )
        detach();
    else
        moveTo(a);
}

