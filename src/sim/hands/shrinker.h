// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.

#ifndef SHRINKER_H
#define SHRINKER_H

#include "hand.h"
class ShrinkerProp;

/// A Hand that can eat Fibers at their ends
/**

   In the Classic Fiber:
  The speed of induced depolymerization NOT set by @ref ShrinkerPar `chewing_speed', but by @ref 
  FiberPar 'max_chewing_speed". This is because the model in the classic fiber is not a change in 
  catastrophe rate, but immediately induces a moderate state of catastrophe (mapped to STATE_ORANGE),
  with a corresponding fiber-specific shrinking speed.

  This model is inspired from:
  <em>
 <b>Cortical Dynein Controls Microtubule Dynamics to Generate Pulling Forces that Reliably Position 
 Microtubule Asters</b>\n
 Laan, L. et al. Cell. 2012 Feb 3; 148(3): 502–514.
 </em>
 
 

 @ingroup HandGroup
 */
class Shrinker : public Hand
{
private:
    
    /// the fiber end that the Shrinker has reached, or NO_END
    FiberEnd engaged;
    
    /// disabled default constructor
    Shrinker();

public:
    
    /// Property
    ShrinkerProp const* prop;
    
    /// constructor
    Shrinker(ShrinkerProp const*, HandMonitor*);
    
    /// destructor
    ~Shrinker() {}
    
    
    /// attach and update variables
    void   attach(FiberSite const&);

    /// simulate when `this` is attached but not under load
    void   stepUnloaded();
    
    /// simulate when `this` is attached and under load
    void   stepLoaded(Vector const& force, real force_norm);
    
};

#endif

