// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.

#include "capper.h"
#include "capper_prop.h"
#include "common.h"
#include "glossary.h"
#include "exceptions.h"
#include "iowrapper.h"
#include "simul.h"


Capper::Capper(CapperProp const* p, HandMonitor* h)
: Hand(p,h), prop(p)
{
}

/**
 Test for spontaneous detachment using Gillespie approach.
    @return true if the test has passed, and detach() was called.
    see @ref Stochastic
    */
bool Capper::testDetachment()
{
    nextDetach -= prop->unbinding_rate_dt;
    
    if ( nextDetach <= 0 )
    {
        if (fbAbs + prop->set_speed_dt  <= fbFiber->abscissaM())
            fiber()->setDynamicStateM(prop->release_state);
        else if (fbAbs + prop->set_speed_dt  >= fbFiber->abscissaP())
            fiber()->setDynamicStateP(prop->release_state);
        detach();
        return true;
    }
    
    return false;
}


/**
 Test for spontaneous detachment using Gillespie approach.
    @return true if the test has passed, and detach() was called.
    see @ref Stochastic
    */
bool Capper::testKramersDetachment(const real force)
{
    /*
        Attention: the exponential term can easily become numerically "infinite",
        which is problematic if 'unbinding_rate==0' and 'unbinding_force' is finite.
        This issue is handled in HandProp::complete()
        */
    nextDetach -= prop->unbinding_rate_dt * exp(force*prop->unbinding_force_inv);
    if ( nextDetach <= 0 )
    {
        if (fbAbs + prop->set_speed_dt  <= fbFiber->abscissaM())
            fiber()->setDynamicStateM(prop->release_state);
        else if (fbAbs + prop->set_speed_dt >= fbFiber->abscissaP())
            fiber()->setDynamicStateP(prop->release_state);
        detach();
        return true;
    }
    return false;
}

void Capper::stepUnloaded()
{
    
    assert_true( attached() );
    real a = fbAbs + prop->set_speed_dt;

    if ( a <= fbFiber->abscissaM() )
    {
        if (fbAbs > fbFiber->abscissaM() + REAL_EPSILON ){
            nextDetach = RNG.exponential();
        }
        a = fbFiber->abscissaM();
        if  (!(fbFiber->dynamicStateM() == STATE_RED))
            fiber()->setDynamicStateM(STATE_WHITE);
    }
    
    if ( a >= fbFiber->abscissaP() )
    {
        if (fbAbs < fbFiber->abscissaP() - REAL_EPSILON ){
            nextDetach = RNG.exponential();
        }
        a = fbFiber->abscissaP();
        if (!(fbFiber->dynamicStateP() == STATE_RED))
            fbFiber->setDynamicStateP(STATE_WHITE);
    }

    if(!testDetachment())
        moveTo(a);
}


void Capper::stepLoaded(Vector const& force, real force_norm)
{
    assert_true( attached() );
    
    // the load is the projection of the force on the local direction of Fiber
    real load = dot(force, dirFiber());
    
    // calculate load-dependent displacement:
    real dab = prop->set_speed_dt + load * prop->var_speed_dt;

    // possibly limit the range of the speed:
    if ( prop->limit_speed )
    {
        dab = std::max(dab, prop->min_dab);
        dab = std::min(dab, prop->max_dab);
    }
    
    real a = fbAbs + dab;
    
    if ( a <= fbFiber->abscissaM() )
    {
        a = fbFiber->abscissaM();
        if (!(fbFiber->dynamicStateM() == STATE_RED))
            fiber()->setDynamicStateM(STATE_WHITE);
    }
    
    if ( a >= fbFiber->abscissaP() )
    {
        a = fbFiber->abscissaP();
        if (!(fbFiber->dynamicStateP() == STATE_RED))
            fiber()->setDynamicStateP(STATE_WHITE);
    }
    

    
    if ( !testKramersDetachment(force_norm) )
       moveTo(a);

    //std::cerr << this << " > " << fbAbs << "  " << dab << "\n";
}

