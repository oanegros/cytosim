// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.

#ifndef SHRINKER_PROP_H
#define SHRINKER_PROP_H

#include "hand_prop.h"


/// Additional Property for Shrinker
/**
 @ingroup Properties
 */
class ShrinkerProp : public HandProp
{
    friend class Shrinker;
    
public:
    
    /**
     @defgroup ShrinkerPar Parameters of Shrinker
     @ingroup Parameters
     Inherits @ref HandPar.
     @{
     */
    
    /// length of Fiber eaten per unit time, once the engaged with an end.
    real    chewing_speed;
    
    /// unidimensional diffusion coefficient while bound to the Fiber
    real    diffusion;
    
    real    stall_force;
    /// @}
    
private:
    
    // real chewing_speed_dt;

    real diffusion_dt, mobility_dt;
    
    /// limits for a displacement in one time_step apply if ( limit_speed = true )
    real    min_dab, max_dab;

    /// variables derived from `unloaded_speed`
    real    set_speed_dt, abs_speed_dt, var_speed_dt;

public:
    
    /// constructor
    ShrinkerProp(const std::string& n) : HandProp(n)  { clear(); }
    
    /// destructor
    ~ShrinkerProp() { }
    
    /// return a Hand with this property
    virtual Hand * newHand(HandMonitor*) const;
    
    /// set default values
    void clear();
    
    /// set from a Glossary
    void read(Glossary&);
    
    /// compute values derived from the parameters
    void complete(Simul const&);
    
    /// perform additional tests for the validity of parameters, given the elasticity
    void checkStiffness(real stiff, real len, real mul, real kT) const;

    /// return a carbon copy of object
    Property* clone() const { return new ShrinkerProp(*this); }

    /// write all values
    void write_values(std::ostream&) const;
    
};

#endif

