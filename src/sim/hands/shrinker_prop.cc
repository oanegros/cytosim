// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.

#include "dim.h"
#include "exceptions.h"
#include "glossary.h"
#include "common.h"
#include "property_list.h"
#include "simul_prop.h"
#include "shrinker_prop.h"
#include "shrinker.h"
#include "simul.h"


Hand * ShrinkerProp::newHand(HandMonitor* m) const
{
    return new Shrinker(this, m);
}


void ShrinkerProp::clear()
{
    HandProp::clear();

    chewing_speed = 0;
    diffusion     = 0;
    stall_force = 0;
}


void ShrinkerProp::read(Glossary& glos)
{
    HandProp::read(glos);
    
    glos.set(chewing_speed, "chewing_rate");
    glos.set(chewing_speed, "chewing_speed");
    glos.set(chewing_speed, "shrinking_speed");
    glos.set(stall_force, "stall_force");
    glos.set(diffusion,     "diffusion");
}


void ShrinkerProp::complete(Simul const& sim)
{
    HandProp::complete(sim);
    
    if ( chewing_speed > 0 )
        throw InvalidParameter("chewer:chewing_speed must be <= 0");

    
    if ( sim.ready() && stall_force <= 0 )
        throw InvalidParameter("motor:stall_force must be > 0");
    
    set_speed_dt = sim.time_step() * chewing_speed;
    abs_speed_dt = fabs(set_speed_dt);
    var_speed_dt = abs_speed_dt / stall_force;
    
    // The limits for a displacement in one time_step apply if ( limit_speed = true )
    if ( chewing_speed > 0 )
    {
        min_dab = 0;
        max_dab = 2 * sim.time_step() * chewing_speed;
    }
    else
    {
        min_dab = 2 * sim.time_step() * chewing_speed;
        max_dab = 0;
    }
    
    if ( diffusion < 0 )
        throw InvalidParameter("chewer:diffusion must be >= 0");

    /*
     We want for one degree of freedom to fulfill `var(dx) = 2 D dt`
     And we use: dx = diffusion_dt * RNG.sreal()
     Since `sreal()` is uniformly distributed, its variance is 1/3,
     and we need `diffusion_dt^2 = 6 D dt`
     */
    diffusion_dt = sqrt( 6.0 * diffusion * sim.time_step() );
    
    // use Einstein's relation to get a mobility:
    mobility_dt = diffusion * sim.time_step() / sim.prop->kT;
    
    // std::clog << " Shrinker `" << name() << "' has mobility = " << diffusion / sim.prop->kT << "\n";
}


void ShrinkerProp::checkStiffness(real stiff, real len, real mul, real kT) const
{
    HandProp::checkStiffness(stiff, len, mul, kT);
    
    /*
     Compare mobility with stiffness: this can induce instability
     */
    real a = mobility_dt * stiff * mul;
    if ( a > 1.0 )
    {
        InvalidParameter e("unstable shrinker\n");
        e << "simulating `" << name() << "' may fail as:\n";
        e << PREF << "mobility = " << diffusion / kT << '\n';
        e << PREF << "mobility * stiffness * time_step = " << a << '\n';
        e << PREF << "-> reduce time_step (really)\n";
        throw e;
    }
    
}


void ShrinkerProp::write_values(std::ostream& os) const
{
    HandProp::write_values(os);
    write_value(os, "chewing_speed", chewing_speed);
    write_value(os, "diffusion",     diffusion);
}

